pragma solidity ^0.5.0;

contract GVTcontrole{
    address GVTAddress;
    
    struct AddMuf{
      string companyname;
      uint  regNo;
      string product;
      string place;
       
    }
    
    mapping(uint=>AddMuf)public mufd;
    
    function setmufdetails(string memory _companyname, uint _regNo,string memory _product,string memory _place) public {
        mufd[_regNo] = AddMuf(_companyname,_regNo,_product,_place);
    }
    
    function getmufdetails(uint _regNo) public view returns(string memory _companyname,uint,string memory _product) {
        _companyname = mufd[_regNo].companyname;
        _regNo      = mufd[_regNo].regNo;
        _product   = mufd[_regNo].product;
        
    }
    
}

// Manufactor contract

contract medcine is GVTcontrole {
    address manufactorAddress;
    
bool display ;

     uint public month = 1 minutes;
     
   

  constructor() public {
    manufactorAddress = msg.sender;
    
  }
  struct medcineDetails{
      
    //   string companyname;
      string genericname;
      uint MNTH;
      uint EXP;
      uint MFG;
      uint MRP;

  }
  mapping(address => uint ) public owner;
  mapping(uint => medcineDetails) public med;
   medcineDetails public MedRecord;

  modifier onlymanufactor(){
    require(manufactorAddress == msg.sender,"only owner");
    _;
  }
  
function setMedicineDetails(uint _batchid, string memory _genericname, uint256 _MNTH, uint _EXP , uint _MFG, uint _MRP) public onlymanufactor{
  _MNTH = _MNTH * month;
  _MFG = now;
  _EXP = _MFG + _MNTH;
  med[_batchid] = medcineDetails(_genericname,_MNTH,_EXP,_MFG,_MRP);
}

function getMedicineDetails(uint _batchid,uint _regNo) public view returns (string memory _companyname, string memory _genericname, uint _EXP , uint _MFG, uint _MRP){
 
  _companyname = mufd[_regNo].companyname;
  _genericname = med[_batchid].genericname;
  _MFG = med[_batchid].MFG;
  _EXP = med[_batchid].EXP;
  _MRP = med[_batchid].MRP;

}


}

contract BILLING is medcine{
    address shopeaddress;
    
    
    struct BillDetails{
        string shopename;
    
        
    }
    
    mapping(uint => BillDetails) public DBL;
    mapping (address => uint) public shop;
    
    
    function setSHPD(uint _shopeid,string memory _shopename) public{
    
    DBL[_shopeid] = BillDetails(_shopename);
        
    }
    
    function getdetails(uint _batchid,uint _shopeid,uint _regNo) public   view returns (string memory _shopename, string memory _companyname, string memory _genericname, uint _EXP , uint _MFG, uint _MRP){
       require(now < med[_batchid].EXP );
        _shopename  = DBL[_shopeid].shopename;
      _companyname = mufd[_regNo].companyname;
        _genericname = med[_batchid].genericname;
        _MFG = med[_batchid].MFG;
        _EXP = med[_batchid].EXP;
        _MRP = med[_batchid].MRP;
        
    }
   
      function medicineexpired(uint batchid) public view returns(bool status){

        status = now < (med[batchid].EXP) ;
    }
    
  
}